import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import "./index.css";
import * as serviceWorker from "./serviceWorker";
import { ApiTodosProvider, LocalStorageTodosProvider, UrlTodosProvider } from "./shared";
import HistoryProvider from "./history";

ReactDOM.render(
  <>
    <ApiTodosProvider>
      <h1>API todos</h1>
      <App />
    </ApiTodosProvider>
    <LocalStorageTodosProvider>
      <h1>Local storage todos</h1>
      <App />
    </LocalStorageTodosProvider>
    <HistoryProvider>
      <UrlTodosProvider>
        <h1>URL todos</h1>
        <App />
      </UrlTodosProvider>
    </HistoryProvider>
  </>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
