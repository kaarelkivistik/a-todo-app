import { createBrowserHistory } from "history";
import React, { createContext, useContext, useEffect, useState } from "react";

const HistoryContext = createContext();
const history = createBrowserHistory();

const HistoryProvider = props => {
  const [location, setLocation] = useState(history.location);

  useEffect(() => {
    return history.listen(() => setLocation(history.location));
  }, []);

  return <HistoryContext.Provider {...props} value={[location, history]} />;
};

export const useLocation = () => useContext(HistoryContext)[0];
export const useHistory = () => useContext(HistoryContext)[1];

export default HistoryProvider;
