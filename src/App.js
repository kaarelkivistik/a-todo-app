import React, { useState } from "react";
import "./App.css";
import { useAllTodos, useCreateTodo, useDeleteTodo, useUpdateTodo } from "./shared";

const App = () => (
  <>
    <Title />
    <TodoList />
  </>
);

const Title = () => {
  const [numberOfCompletedTodos, numberOfTodos] = useTodoCount();

  return (
    <h2>
      {numberOfCompletedTodos} completed out of {numberOfTodos} todo(s)
    </h2>
  );
};

const useTodoCount = () => {
  const todos = useAllTodos();

  const numberOfTodos = todos.length;
  const numberOfCompletedTodos = todos.filter(todo => todo.done).length;

  return [numberOfCompletedTodos, numberOfTodos];
};

const TodoList = () => {
  const [completed, notCompleted] = useGroupedTodos();

  return (
    <ul>
      {completed.map(todo => (
        <TodoItem key={todo.id} {...todo} />
      ))}
      {notCompleted.length && completed.length ? <hr /> : null}
      {notCompleted.map(todo => (
        <TodoItem key={todo.id} {...todo} />
      ))}
      <NewTodoItem />
    </ul>
  );
};

const useGroupedTodos = () => {
  const todos = useAllTodos();

  const notCompletedTodos = todos.filter(todo => !todo.done);
  const completedTodos = todos.filter(todo => todo.done);

  return [notCompletedTodos, completedTodos];
};

const TodoItem = ({ id, text, done }) => {
  const updateTodo = useUpdateTodo();
  const deleteTodo = useDeleteTodo();

  const onDoneChange = event =>
    updateTodo(id, {
      id,
      text,
      done: event.target.checked
    });

  const onClickDelete = () => deleteTodo(id);

  return (
    <li>
      <label>
        <input type="checkbox" checked={done} onChange={onDoneChange} />
        <span style={{ textDecoration: done ? "line-through" : "none", margin: "0 0.4rem" }}>{text}</span>
        <button onClick={onClickDelete}>delete</button>
      </label>
    </li>
  );
};

const NewTodoItem = () => {
  const createTodo = useCreateTodo();

  const [text, setText] = useState("");
  const onTextChange = event => setText(event.target.value);

  const onSubmit = event => {
    event.preventDefault();
    createTodo(text);
    setText("");
  };

  return (
    <li>
      <form onSubmit={onSubmit}>
        <input placeholder="take out the trash etc" value={text} onChange={onTextChange} autoFocus />
        <button>Create todo</button>
      </form>
    </li>
  );
};

export default App;
