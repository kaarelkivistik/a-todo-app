import React, { createContext, useContext, useEffect, useState } from "react";
import { useLocation, useHistory } from "./history";
import qs from "qs";

const TodosContext = createContext({
  allTodos: [{ id: 1, text: "this is dummy TodosContext, you probably forgot to use TodosProvider?", done: false }],
  createTodo: () => {},
  updateTodo: () => {},
  deleteTodo: () => {}
});

export const LocalStorageTodosProvider = props => {
  const [todos, setTodos] = useState(localStorage.todos ? JSON.parse(localStorage.todos) : []);

  useEffect(() => {
    localStorage.todos = JSON.stringify(todos);
  }, [todos]);

  const value = {
    allTodos: todos,
    createTodo: text => setTodos(todos => [...todos, createTodoObject(text)]),
    updateTodo: (id, updatedTodo) => setTodos(todos => todos.map(todo => (todo.id === id ? updatedTodo : todo))),
    deleteTodo: id => setTodos(todos => todos.filter(todo => todo.id !== id))
  };

  return <TodosContext.Provider {...props} value={value} />;
};

const createTodoObject = text => ({
  id: new Date().getTime(),
  done: false,
  text
});

export const ApiTodosProvider = props => {
  const [todos, setTodos] = useState([]);

  const refreshTodos = () =>
    fetch("/todos")
      .then(response => response.json())
      .then(setTodos);

  useEffect(() => {
    refreshTodos();
  }, []);

  const value = {
    allTodos: todos,

    createTodo: text =>
      fetch("/todos", {
        method: "POST",
        headers: {
          "content-type": "application/json"
        },
        body: JSON.stringify({ text })
      }).then(refreshTodos),

    updateTodo: (id, todo) =>
      fetch(`/todos/${id}`, {
        method: "PUT",
        headers: {
          "content-type": "application/json"
        },
        body: JSON.stringify(todo)
      }).then(refreshTodos),

    deleteTodo: id =>
      fetch(`/todos/${id}`, {
        method: "DELETE"
      }).then(refreshTodos)
  };

  return <TodosContext.Provider {...props} value={value} />;
};

export const UrlTodosProvider = props => {
  const location = useLocation();
  const history = useHistory();

  const { todos = [] } = qs.parse(location.search, {
    allowDots: true,
    ignoreQueryPrefix: true,
    decoder: (value, decoder) => (value === "true" ? true : value === "false" ? false : decoder(value))
  });

  const setTodos = todos =>
    history.replace(
      qs.stringify(
        { todos },
        {
          allowDots: true,
          addQueryPrefix: true
        }
      )
    );

  const value = {
    allTodos: todos,
    createTodo: text => setTodos([...todos, createTodoObject(text)]),
    updateTodo: (id, updatedTodo) => setTodos(todos.map(todo => (todo.id === id ? updatedTodo : todo))),
    deleteTodo: id => setTodos(todos.filter(todo => todo.id !== id))
  };

  return <TodosContext.Provider {...props} value={value} />;
};

const useTodos = () => useContext(TodosContext);

export const useAllTodos = () => useTodos().allTodos;
export const useCreateTodo = () => useTodos().createTodo;
export const useUpdateTodo = () => useTodos().updateTodo;
export const useDeleteTodo = () => useTodos().deleteTodo;
